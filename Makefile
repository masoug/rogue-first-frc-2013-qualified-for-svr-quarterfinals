# These variables control the compiler and linker flags. Change them as
# appropriate.

# FIRST specific stuff.
ROBOT_ADDRESS = 10.1.14.2
TARGET = FRC_UserProgram
WPILIB_INCLUDE_PATH:=./WPILib
#WPILIB_SEARCH_PATH:=$(WPILIB_INCLUDE_PATH)
WPILIB_SEARCH_PATH:=$(WPILIB_INCLUDE_PATH)/PPC603gnu/WPILib/Debug

# Force wine to be quiet.
export WINEDEBUG=-all
# Configuration for development on FIRST FRC platforms.
WINE = false
ifeq ($(WINE), true)
export GCCPATH=/home/robotics/gccdist
export WIND_BASE=$(GCCPATH)/WindRiver/vxworks-6.3
export WIND_HOME=$(GCCPATH)/WindRiver
export WIND_LIC_PROXY=$(GCCPATH)/WindRiver/setup/x86-win32/bin
export LM_LICENSE_FILE=$(GCCPATH)/supp/zwrsLicense.lic
export WIND_BIN=$(GCCPATH)/WindRiver/gnu/3.4.4-vxworks-6.3/x86-win32/bin
#export PATH=$PATH:$GCCPATH/wrappers
CXXPPC = wine $(WIND_BIN)/c++ppc.exe
CCPPC = wine $(WIND_BIN)/ccppc.exe
NMPPC = wine $(WIND_BIN)/nmppc.exe
else
CXXPPC = c++ppc
CCPPC = ccppc
NMPPC = nmppc
endif

# List of source code.
#MODULES_SRC := Modules/DriveModule.cpp Modules/RobotModule.cpp Modules/Shooter.cpp Modules/Climber.cpp
#MODULES_SRC := Modules/DriveModule.cpp Modules/RobotModule.cpp Modules/Shooter.cpp
#MODULES_SRC := Modules/DriveModule.cpp Modules/RobotModule.cpp Modules/VelocityProfile.cpp
MODULES_SRC := Modules/DriveModule.cpp Modules/RobotModule.cpp Modules/VelocityProfile.cpp Modules/Collector.cpp Modules/Shooter.cpp

XBOX_SRC := Xbox.cpp

AUI_CPP_SRC := AuI/AuI.cpp AuI/AutoController.cpp
AUI_SRC := AuI/lex.yy.c
C_SOURCE := $(AUI_SRC)
CPP_SOURCE := $(MODULES_SRC) $(AUI_CPP_SRC) $(XBOX_SRC) main.cpp Bezier.cpp
#CPP_SOURCE := TEST.cpp

# Everything after this line should not need to be modified for
# basic compilation. However, significant changes to the build structure
# will probably involve modifying these lines.

# Includes you'll need to include.
ADDED_INCLUDES = -I$(WPILIB_INCLUDE_PATH)

DEBUG_MODE = 1

ADDED_CFLAGS =

ifeq ($(DEBUG_MODE), 1)
OBJ_DIR := PPC603gnu_DEBUG
CFLAGS = -g -mlongcall
else
OBJ_DIR := PPC603gnu
CFLAGS = -Os -fstrength-reduce -fno-builtin -fno-strict-aliasing -mlongcall
endif

LINKFLAGS = $(CFLAGS)

# List all the *compiled* object files here, under the OBJ_DIR
# directory. Make will automatically locate the source file and
# compile it.
C_OBJ := $(C_SOURCE:%.c=%.o)
CPP_OBJ := $(CPP_SOURCE:%.cpp=%.o)
OBJECTS := $(foreach CPP_OBJ, $(CPP_OBJ), $(OBJ_DIR)/$(CPP_OBJ)) $(foreach C_OBJ, $(C_OBJ), $(OBJ_DIR)/$(C_OBJ))

# This is the name of the output shared library.
PROJECT_TARGETS := $(OBJ_DIR)/$(TARGET).out

# If you have other VxWorks .a files to reference, list them here.
LIBS = $(WPILIB_SEARCH_PATH)/WPILib.a 
LIBPATH =

# Everything after this line should not need to be modified for
# basic compilation. However, significant changes to the build structure
# will probably involve modifying these lines.

WIND_BASE := $(subst \,/,$(WIND_BASE))

CPU = PPC603
TOOL_FAMILY = gnu
TOOL = gnu
CC_ARCH_SPEC = -mcpu=603 -mstrict-align -mno-implicit-fp

IDE_INCLUDES = -I$(WIND_BASE)/target/h -I$(WIND_BASE)/target/h/wrn/coreip 

# This basic rule compiles a .c file into a .o file. It can be adapted to
# all other source files that gcc can compile, including assembly (.s) and
# C++ (.cpp, .cc, .C, .cxx) files. To enable support for those extensions,
# copy this rule and modify its extension and compile flags for the
# required source file type.
$(OBJ_DIR)/%.o : %.c
	@echo "Compiling $<"
	@$(CCPPC) $(CFLAGS) $(CC_ARCH_SPEC) -ansi  -Wall  -MD -MP -mlongcall $(ADDED_CFLAGS) $(IDE_INCLUDES) $(ADDED_INCLUDES) -DCPU=$(CPU) -DTOOL_FAMILY=$(TOOL_FAMILY) -DTOOL=$(TOOL) -D_WRS_KERNEL   $(DEFINES) -o "$@" -c "$<"

# Adapted rule for .cpp files
$(OBJ_DIR)/%.o: %.cpp
	@echo "Compiling $<"
	@$(CXXPPC) $(CFLAGS) $(CC_ARCH_SPEC) -ansi  -Wall  -MD -MP -mlongcall $(ADDED_CFLAGS) $(IDE_INCLUDES) $(ADDED_INCLUDES) -DCPU=$(CPU) -DTOOL_FAMILY=$(TOOL_FAMILY) -DTOOL=$(TOOL) -D_WRS_KERNEL $(DEFINES) -o "$@" -c "$<"

all : check_objectdir build_aui $(PROJECT_TARGETS) 

$(PROJECT_TARGETS) : $(OBJECTS)
	@echo "Munching and linking $(OBJECTS)";
	@rm -f "$@" ctdt.c;$(NMPPC) $(OBJECTS) | tclsh $(WIND_BASE)/host/resource/hutils/tcl/munch.tcl -c ppc > ctdt.c
	@$(CCPPC) $(LINKFLAGS) $(CC_ARCH_SPEC) -fdollars-in-identifiers -Wall $(ADDED_CFLAGS) $(IDE_INCLUDES) $(ADDED_INCLUDES) -DCPU=$(CPU) -DTOOL_FAMILY=$(TOOL_FAMILY) -DTOOL=$(TOOL) -D_WRS_KERNEL   $(DEFINES)  -o ctdt.o -c ctdt.c
	@$(CCPPC) -r -nostdlib -Wl,-X -T $(WIND_BASE)/target/h/tool/gnu/ldscripts/link.OUT -o "$@" $(OBJECTS) $(LIBPATH) $(LIBS)  $(ADDED_LIBPATH) $(ADDED_LIBS) ctdt.o
	@rm -f ctdt.c
	@echo "Compilation successfully completed."
	@echo; fortune -s
	@echo


check_objectdir :
	@if [ ! -d "$(OBJ_DIR)" ]; then\
		mkdir -p $(OBJ_DIR);\
	fi

clean :
	@echo "Cleaning workspace..."
	@rm -f $(OBJECTS) $(PROJECT_TARGETS) $(wildcard $(OBJ_DIR)/*.unstripped)
	@rm -f $(OBJ_DIR)/*.o $(OBJ_DIR)/*.d $(OBJ_DIR)/Modules/*.o $(OBJ_DIR)/Modules/*.d
	@rm -f $(OBJ_DIR)/inih/*.o $(OBJ_DIR)/inih/*.d $(OBJ_DIR)/inih/cpp/*.d $(OBJ_DIR)/inih/cpp/*.o

.PHONY: deploy reboot doc style
deploy: check_objectdir $(PROJECT_TARGETS)
	@echo "Deploying to robot..."
	@ftpconnect.bat

build_aui:
	@echo "Running flex and yacc..."
	@flex -o AuI/lex.yy.c AuI/AuI.lex
	@bison --verbose --debug -d AuI/AuI.yacc -o AuI/AuI.cpp

reboot:
	@echo "Rebooting robot..."
	@echo "reboot" | nc -u -v $(ROBOT_ADDRESS) 7654

doc:
	@echo "Generating documentation..."
	@doxygen

style:
	@echo "Styling source code..."
	@astyle --style=ansi --mode=c -r $(CPP_SOURCE)
	@echo "Removing originals..."
	@rm -rf *.orig
	@rm -rf Modules/*.orig

.DUMMY: check_objectdir clean
