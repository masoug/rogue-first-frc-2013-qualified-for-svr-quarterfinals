#ifndef BEZIER_H_
#define BEZIER_H_

#include <math.h>

#ifndef M_PI
#define M_PI 3.14159265359
#endif

class BezierCurve {
public:
	BezierCurve(double x0, double y0,
				double x1, double y1,
				double x2, double y2,
				double x3, double y3);
	~BezierCurve();
	
	/* Evaluating functions. */
	double x(double t);
	double y(double t);
	double dx(double t);
	double dy(double t);
	double dsdt(double t);
	double angle(double t);
	double t(double s);
	double arcLength();

private:
	double m_x0, m_y0;
	double m_x1, m_y1;
	double m_x2, m_y2;
	double m_x3, m_y3;
};


#endif
