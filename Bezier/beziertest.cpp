#include <iostream>
#include <math.h>
#include "Bezier.h"

//#define M_PI 3.14159265
#define STEP 0.01

using namespace std;

int main() {
	BezierCurve curve(
						0.0, 0.0,
						0.8, 0.0,
						1.22, 0.56,
						1.22, 1.22);
	cout << "Time,y" << endl;
	double yn = 0.0;
	for (double i = 0.0; i <= 2.0; i += STEP) {
		double k1 = 1.0/curve.dsdt(i);
		double k2 = 1.0/curve.dsdt(yn+(k1/2.0));
		double k3 = 1.0/curve.dsdt(yn+(k2/2.0));
		double k4 = 1.0/curve.dsdt(yn+k3);
		yn = yn + (STEP*(k1+(2.0*k2)+(2.0*k3)+k4)/6.0);
		cout << i << "," << yn << endl;
	}
	//cout << curve.arcLength() << endl;
	return 0;
}