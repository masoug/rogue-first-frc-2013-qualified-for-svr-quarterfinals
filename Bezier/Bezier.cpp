#include "Bezier.h"

#define STEP 0.001 // adjust for needed precision/time

BezierCurve::BezierCurve(double x0, double y0,
						double x1, double y1,
						double x2, double y2,
						double x3, double y3)
{
	m_x0 = x0;
	m_y0 = y0;
	m_x1 = x1;
	m_y1 = y1;
	m_x2 = x2;
	m_y2 = y2;
	m_x3 = x3;
	m_y3 = y3;
}

BezierCurve::~BezierCurve() {
	// destructor...
}


double BezierCurve::x(double t) {
	if (t < 0.0)
		return m_x0;
	if (t > 1.0)
		return m_x3;
	double one_minus_t = 1.0-t;
	double one_minus_t2 = one_minus_t*one_minus_t;
	double one_minus_t3 = one_minus_t2*one_minus_t;
	double t2 = t*t;
	double t3 = t2*t;

	return (one_minus_t3*m_x0)+(3.0*one_minus_t2*t*m_x1)+(3.0*one_minus_t*t2*m_x2)+(t3*m_x3);
}

double BezierCurve::y(double t) {
	if (t < 0.0)
		return m_y0;
	if (t > 1.0)
		return m_y3;
	double one_minus_t = 1.0-t;
	double one_minus_t2 = one_minus_t*one_minus_t;
	double one_minus_t3 = one_minus_t2*one_minus_t;
	double t2 = t*t;
	double t3 = t2*t;

	return (one_minus_t3*m_y0)+(3.0*one_minus_t2*t*m_y1)+(3.0*one_minus_t*t2*m_y2)+(t3*m_y3);
}

double BezierCurve::dx(double t) {
	// if (t < 0.0 || t > 1.0)
	// 	return 0.0;
	double t2 = t*t;
	return (-3.0*m_x0*t2)+(6.0*m_x0*t)-(3.0*m_x0)+(9.0*m_x1*t2)-(12.0*m_x1*t)+(3.0*m_x1)-(9*m_x2*t2)+(6.0*m_x2*t)+(3.0*m_x3*t2);
}

double BezierCurve::dy(double t) {
	// if (t < 0.0 || t > 1.0)
	// 	return 0.0;
	double t2 = t*t;
	return (-3.0*m_y0*t2)+(6.0*m_y0*t)-(3.0*m_y0)+(9.0*m_y1*t2)-(12.0*m_y1*t)+(3.0*m_y1)-(9*m_y2*t2)+(6.0*m_y2*t)+(3.0*m_y3*t2);
}

double BezierCurve::dsdt(double t) {
	return sqrt((dx(t)*dx(t))+(dy(t)*dy(t)));
}

double BezierCurve::angle(double t) {
	return atan2(dy(t), dx(t))*180.0/M_PI;
}

double BezierCurve::t(double s) {
	double yn = 0.0;
	for (double i = 0.0; i <= s; i += STEP) {
		double k1 = 1.0/dsdt(i);
		double k2 = 1.0/dsdt(yn+(k1/2.0));
		double k3 = 1.0/dsdt(yn+(k2/2.0));
		double k4 = 1.0/dsdt(yn+k3);
		yn = yn + (STEP*(k1+(2.0*k2)+(2.0*k3)+k4)/6.0);
	}
	return yn;
}

double BezierCurve::arcLength() {
	double length = 0.0;
	for (double x = 0.0; x <= 2.0; x += STEP) {
		double k1 = dsdt(x);
		double k2 = dsdt(x+(STEP/2.0));
		double k3 = dsdt(x+(STEP/2.0));
		double k4 = dsdt(x+STEP);
		length = length + (STEP*(k1+(2.0*k2)+(2.0*k3)+k4)/6.0);
	}
	return length;
}
