#ifndef COMMANDS_H_
#define COMMANDS_H_

#include <string>
#include "Modules/VelocityProfile.h"
#include "Bezier.h"

/** Common command types.
* Command flags that determine which command the AUI system should execute.
*/
enum CommandType {
	IDLE, /*!< Tells the robot to maintain its current state. Other components may be active.*/
	DRIVE, /*!< Commands the AUI to drive with the given parameters. */
	SHOOTER, /*!< Instructs the robot to turn on the shooter wheels. */
	FIRE, /*!< Extends the piston that pushes the frisbee into the shooter. */
	ROTATE, /*!< Turns the robot to a specific angle relative to the robot's original position. */
	DONE, /*!< Clears the robot's command queue. */
	WAIT, /*!< Waits for a specified number of seconds. */
	COLLECT, /*!< Turns on the intake. */
	CURVE /*!< Experimental curved driving with Bezier Curves. */
};

/** Abstract class for all commands.
*/
class Instruction {
public:
	CommandType type; /*!< All commands must speficy a command type. */

};

/** Drive command.
* Tells the robot to drive to a certain distance at a certain maximum velocity.
* This command utilizes the trapezoidal velocity profile. See VelocityProfile.
* This command "blocks" the thread of execution until the robot travels the specified distance.
* All units are metric!
*/
class DriveCommand : public Instruction {
public:
	/** Constructor. */
	DriveCommand() { type = DRIVE; }
	double distance; /*!< Distance in meters the robot must drive. */
	double velocity; /*!< The veolcity (in meters/second) the robot's maximum speed. */
};

/**
* Turns the shooting wheels on or off.
*/
class ShooterCommand : public Instruction {
public:
	/** Constructor. */
	ShooterCommand() { type = SHOOTER; }
	bool on; /*!< Flag to tell whether the shooter wheels should be on or off. */
};

/**
* Tells the robot to extend or retract the piston that pushes the frisbee into the shooter.
*/
class FireCommand : public Instruction {
public:
	/** Constructor. */
	FireCommand() {	type = FIRE; }
	bool fire; /*!< Flag to tell whether to extend or retract the piston that pushes the frisbee into the shooter. */
};

/** Tells the robot to turn to a certain angular position.
* The angle is relative to the last time the gyro reset. Depending on the 
* gyro, clockwise is positive counter-clockwise is negative.
* This command is "blocking".
*/
class RotateCommand : public Instruction {
public:
	/** Constructor. */
	RotateCommand() { type = ROTATE; }
	double degrees; /*!< The extent of which the robot needs to turn. */
};

/** Command that clears the command queue.
*/
class DoneCommand : public Instruction {
public:
	/** Constructor. */
	DoneCommand() { type = DONE; }
};

/** Stops the thread of execution for a set amount of time. */
class AuIWaitCommand : public Instruction {
public:
	/** Constructor. */
	AuIWaitCommand() { type = WAIT; }
	double time; /*!< Time in seconds the AUI system needs to wait. */
};

/** Turns on/off the floor pickup. */
class CollectCommand : public Instruction {
public:
	/** Constructor. */
	CollectCommand() { type = COLLECT; }
	bool collect; /*!< Flag to turn on or off the collector. */
};

/** Experimental curve command that uses Bezier curves.
* Not recommended for use. See BezierCurve and VelocityProfile.
*/
class CurveCommand : public Instruction {
public:
	/** Constructor.
	* @param maxSpeed Maximum speed for the velocity profile.
	* Parameters x0 ... x3 and y0 ... y3 are the coordinates for the control points of the Bezier curve. */
	CurveCommand(double maxSpeed, double x0, double y0, double x1, double y1, double x2, double y2, double x3, double y3) :
		curve(x0, y0, x1, y1, x2, y2, x3, y3), curveArcLength(curve.arcLength()), profile(curveArcLength, maxSpeed)
	{ type = CURVE; }

	BezierCurve curve; /*!< Curve calculator. */
	double curveArcLength; /*!< Length of the curve. Calculated from the Bezier curve. */
	VelocityProfile profile; /*!< Velocity profile that controls the speed of the robot as it travels the curve. */
};

#endif
