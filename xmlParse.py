import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt
from math import sqrt
import sys

class Group(object):
	pass

if len(sys.argv) < 2:
	print "Usage:"
	print "\txmlParse.py <file name>"
	sys.exit(-1)

print "Reading datafile..."
logFile = ET.parse(sys.argv[1])
match = logFile.getroot()

time = list()
battery = list()
modes = Group()
modules = Group()

print "Processing data..."
for entry in match:
	time.append(float(entry.attrib['time']))
	battery.append(float(entry.attrib['battery']))
	#autoMode.append(int(entry.attrib['isAuto']))
	#teleopMode.append(int(entry.attrib['isTeleop']))
	for group in entry:
		# if the group is a module
		if group.tag == 'module':
			moduleName = group.attrib['name']
			moduleDict = modules.__dict__
			if not (moduleName in moduleDict.keys()):
				moduleDict[moduleName] = Group()
			moduleObject = moduleDict[moduleName]
			for stream in group:
				streamName = stream.attrib['name']
				streamValue = float(stream.attrib['value'])
				if not (streamName in moduleObject.__dict__.keys()):
					moduleObject.__dict__[streamName] = list()
				moduleObject.__dict__[streamName].append(streamValue)
		#if the group is other; teleop, auto, etc...
		else:
			modeName = group.tag
			modeDict = modes.__dict__
			if not (modeName in modeDict.keys()):
				modeDict[modeName] = Group()
			modeObject = modeDict[modeName]
			for stream in group:
				if stream.tag == 'stream':
					streamName = stream.attrib['name']
					streamValue = float(stream.attrib['value'])
					if not (streamName in modeObject.__dict__.keys()):
						modeObject.__dict__[streamName] = list()
					modeObject.__dict__[streamName].append(streamValue)

def averageBattery():
	average = float()
	for sample in battery:
		average += sample
	average /= len(battery)
	return average

def stdDeviationBattery():
	stdDev = float()
	avg = averageBattery()
	for sample in battery:
		stdDev += (sample-avg)**2
	stdDev /= (len(battery)-1)
	stdDev = sqrt(stdDev)
	return stdDev

print "Done processing..."
print "============= ROBOT MATCH SUMMARY ================="
print "Match duration:", time[len(time)-1]-time[0], "seconds."
print "Average battery voltage:", averageBattery(), "volts."
print "Battery voltage standard deviation:", stdDeviationBattery()
print "\nUse iPython's interactive features to analyze/plot the data found in the following variables: time, battery, modes, modules"
print "=================== DONE =========================="