#include "Rogue.h"
#include <assert.h>
#include <math.h>

//#define M_PI 3.14159265358979323

Rogue::Rogue(void) :
airCompressor(COMPRESSOR, PRESSURE), 
wheel(2), throttle(1), xBox(3), controller(4)
{
	std::cout << "R    O    G    U    E" << std::endl;
    dLCD = DriverStationLCD::GetInstance();
    ROGUE_LCD("Initializing...")

    std::cout << "Initializing robot modules..." << std::endl;

    std::cout << "Starting drive module..." << std::endl;
    drive = new DriveModule(LEFT_FRONT, LEFT_BACK, RIGHT_FRONT, RIGHT_BACK,
        ENCODER_L_A, ENCODER_L_B, ENCODER_R_A, ENCODER_R_B,
        GEAR);
    drive->setTune(1.0, -1.0);
    drive->setVelocityPD(30.4688, 1.01563);
    drive->setAnglePD(0.0425781, 0.078125);

    std::cout << "Starting shooter module..." << std::endl;
    shtr = new Shooter(DISPATCH, SHOOTER_A, SHOOTER_B);

    std::cout << "Starting auto controller..." << std::endl;
    autoControl = new AutoController();
    autoInstruction = NULL;
    autoState = IDLE;

    std::cout << "Victor and solenoid..." << std::endl;
    intake = new Victor(INTAKE);
    bottomIntake = new Relay(BOTTOM_INTAKE);
    bottomIntakeDirection = Relay::kReverse;
    // shooterA = new SHOOTER_MOTORS(SHOOTER_A);
    // shooterB = new SHOOTER_MOTORS(SHOOTER_B);
    climber = new Solenoid(CLIMBER);
    // dispatch = new Solenoid(DISPATCH);

    airCompressor.Start();
    std::cout << "END OF CONSTRUCTOR!!!" << std::endl;
}

void Rogue::Autonomous() {
    static double heading = 0.0;
    if (!IsEnabled())
        heading = 0.0;
    static Timer timeout;
    timeout.Start();
    drive->enable(IsEnabled());
    drive->manual(false);
    drive->gear(); // lo gear
    // shooterA->SetSafetyEnabled(false);
    // shooterB->SetSafetyEnabled(false);
    intake->SetSafetyEnabled(false);

    if (IsEnabled()) {
        fprintf(logFile, "\t<auto>\n");
        fprintf(logFile, "\t\t<stream name='heading' value='%f' type='float' />\n", heading);
        fprintf(logFile, "\t\t<stream name='state' value='%d' type='enum' />\n", (int)autoState);
        fprintf(logFile, "\t\t<stream name='timeout' value='%f' type='float' />\n", timeout.Get());
        fprintf(logFile, "\t</auto>\n");
    }

    if (IsEnabled()) {
        if (autoState == IDLE) {
            // check for new commands
            timeout.Reset();
            drive->setVector(0.0, heading);
            if (!autoControl->empty()) {
                autoInstruction = autoControl->pop();
                autoState = autoInstruction->type;
            }
        } else if (autoState == DRIVE) {
            DriveCommand *cmd = (DriveCommand*)autoInstruction;
            VelocityProfile profile(fabs(cmd->distance), cmd->velocity);
            if (cmd->distance < 0.0)
                drive->setVector(-profile.eval(timeout.Get()), heading);
            else
                drive->setVector(profile.eval(timeout.Get()), heading);
            if (timeout.Get() > profile.totalTime()) {
                autoState = IDLE; // on target, we're done.
                delete autoInstruction; // avoid memory problems...
            }
        } else if (autoState == SHOOTER) {
            if (((ShooterCommand*)autoInstruction)->on) {
                shtr->wheelStart();
            } else {
                shtr->wheelStop();
            }
            autoState = IDLE;
            delete autoInstruction;
        } else if (autoState == FIRE) {
            if(((FireCommand*)autoInstruction)->fire) {
                shtr->pistonExtend();
            } else {
                shtr->pistonRetract();
            }
            autoState = IDLE; // frisbee left, we're done
            delete autoInstruction;
        } else if (autoState == ROTATE) {
            heading = ((RotateCommand*)autoInstruction)->degrees;
            drive->setVector(0.0, heading);
            // check if we are at the right heading.
            if (drive->getAngleError() < 3.0 || timeout.Get() >= 1.75) {
                autoState = IDLE; // facing the right direction, we're done.
                delete autoInstruction;
            }
        } else if (autoState == DONE) {
            autoControl->clear();
            autoState = IDLE;
            delete autoInstruction;
        } else if (autoState == WAIT) {
            // wait and do nothing
            if (timeout.Get() >= ((AuIWaitCommand*)autoInstruction)->time) {
                autoState = IDLE;
                delete autoInstruction;
            }
        } else if (autoState == COLLECT) {
            if (((CollectCommand*)autoInstruction)->collect) {
                intake->Set(1.0);
                bottomIntake->Set(bottomIntakeDirection);
            } else {
                intake->Set(0.0);
                bottomIntake->Set(Relay::kOff);
            }
            autoState = IDLE;
            delete autoInstruction;
        } else if (autoState == CURVE) {
            CurveCommand *cmd = (CurveCommand*)autoInstruction;
            // pass to drive system...
            double t = cmd->curve.t(drive->getDistance());
            heading = -cmd->curve.angle(t);
            drive->setVector(cmd->profile.eval(timeout.Get()), heading);
            if (timeout.Get() >= cmd->profile.totalTime()) {
                drive->resetDistance();
                autoState = IDLE;
                delete autoInstruction;
            }
        } else {
            // shouldn't get here...
        }
    }
}

void Rogue::Ben(){
    static int i = 0;
    static bool gear = false;
    static bool gearlast = false;

    // shooterA->SetSafetyEnabled(true);
    // shooterB->SetSafetyEnabled(true);
    intake->SetSafetyEnabled(false);
    drive->manual(1);

    if(controller.GetRT())
        intake->Set(1.0);
    else if(controller.GetLT())
        intake->Set(-1.0);

    if(controller.GetA())
        climber->Set(1);
    else if(controller.GetB())
        climber->Set(0);

    // if(controller.GetRB()){
    //     shtr->wheelStart();
    // }else{
    //     shtr->wheelStop();
    // }

    if(controller.GetLB())
        shtr->pistonExtend();
    else 
        shtr->pistonRetract();

    if(controller.GetY() && !gearlast){
        gear = !gear;
        gearlast = true;
    }else if(!controller.GetY() && gearlast){
        gearlast = false;
    }else{

    }

    drive->gear(gear);
    drive->enable(IsEnabled());


    drive->setVector(controller.GetLY(), controller.GetRX());

    if(i % 50 == 0){}

    i++;


}

void Rogue::Test() {
    static int i = 0;
    //drive->setVelocityPD(100*((0.5*wheel.GetZ()) + 0.5), 10*((0.5*throttle.GetZ()) + 0.5));
    //drive->setAnglePD(((0.5*wheel.GetZ()) + 0.5)/10, ((0.5*throttle.GetZ()) + 0.5));

    /* Rotation test */
    // if (throttle.GetTrigger())
    //     drive->setVector(0.0, 180.0);
    // else if (wheel.GetTrigger())
    //     drive->setVector(0.0, -180.0);
    // else
    //     drive->setVector(0.0, 0.0);

    /* Velocity test */
    //drive->setVector(-xBox.GetY(), 0.0);
    //drive->setVector(0.0, 0.0);

    intake->Set(0.0);

    drive->gear();
    drive->enable(IsEnabled());
    drive->manual(false);
    i++;
}



void Rogue::Teleop() {
    static int i = 0;
    intake->SetSafetyEnabled(true);

    // flags
    bool forwardIntake = throttle.GetRawButton(3);
    bool backwardIntake = xBox.GetRawButton(4);
    bool shooterWheel = xBox.GetRawButton(2);
    bool climb = xBox.GetRawButton(8);
    bool fire = xBox.GetRawButton(5) || xBox.GetRawButton(6);
    double wheelOutput = wheel.GetX();
    double wheelNonLinearity = 0.7;
    wheelOutput = sin(M_PI / 2.0 * wheelNonLinearity * wheelOutput) / sin(M_PI / 2.0 * wheelNonLinearity);
    wheelOutput = sin(M_PI / 2.0 * wheelNonLinearity * wheelOutput) / sin(M_PI / 2.0 * wheelNonLinearity);
    wheelOutput = sin(M_PI / 2.0 * wheelNonLinearity * wheelOutput) / sin(M_PI / 2.0 * wheelNonLinearity);
    bool gear = wheel.GetRawButton(8);
    double inputThrottle = -throttle.GetY();
    //double shooterPower = (0.5*throttle.GetZ())+0.5;
    double shooterPower = 1.0;

    // if (i % 50 == 0 && IsEnabled())
    //     std::cout << shooterPower << std::endl;

    if (IsEnabled()) {
        fprintf(logFile, "\t<teleop>\n");
        fprintf(logFile, "\t\t<stream name='forwardIntake' value='%d' type='bool' />\n", (int)forwardIntake);
        fprintf(logFile, "\t\t<stream name='backwardIntake' value='%d' type='bool' />\n", (int)backwardIntake);
        fprintf(logFile, "\t\t<stream name='shooterWheel' value='%d' type='bool' />\n", (int)shooterWheel);
        fprintf(logFile, "\t\t<stream name='climb' value='%d' type='bool' />\n", (int)climb);
        fprintf(logFile, "\t\t<stream name='fire' value='%d' type='bool' />\n", (int)fire);
        fprintf(logFile, "\t\t<stream name='wheelOutput' value='%f' type='float' />\n", wheelOutput);
        fprintf(logFile, "\t\t<stream name='gear' value='%d' type='bool' />\n", (int)gear);
        fprintf(logFile, "\t\t<stream name='inputThrottle' value='%f' type='float' />\n", inputThrottle);
        fprintf(logFile, "\t</teleop>\n");
    }

    /* gearing */
    static bool prevGear = false;
    static int gearCount = 0;
    if (gear != prevGear) {
        gearCount++;
        prevGear = !prevGear;
    }
    if (gearCount % 4 == 0)
        drive->gear(false);
    else if (gearCount % 2 == 0)
        drive->gear(true);

    if (forwardIntake) {
        intake->Set(1.0);
        bottomIntake->Set(bottomIntakeDirection);
    } else if (backwardIntake) {
        intake->Set(-1.0);
        bottomIntake->Set(bottomIntakeDirection);
    } else {
        intake->Set(0.0);
        bottomIntake->Set(Relay::kOff);
    }

    shtr->setPower(shooterPower);
    if (shooterWheel)
        shtr->wheelStart();
    else
        shtr->wheelStop();

    climber->Set(climb);
    if (fire)
        shtr->pistonExtend();
    else
        shtr->pistonRetract();

    drive->setVector(inputThrottle, wheelOutput);
    drive->manual(true);
    drive->enable(IsEnabled());

    i++;
}

void Rogue::RobotMain(void)
{
    std::cout << "Entering main function..." << std::endl;
    ROGUE_LCD("Ready!")
    DriverStation *ds = DriverStation::GetInstance();
    int counter = 0;
    bool prevState = false;
    Timer tmr;
    tmr.Start();

    while (true)
    {
        if (prevState == false && IsEnabled() == true) {
            if (ds->IsFMSAttached())
                logFile = fopen("logFile.dat", "a");
            else
                logFile = fopen("/null", "rw");
            logTime.Start();
            fprintf(logFile, "<match time='%f' FMS='%d' >\n", logTime.Get(), (int)ds->IsFMSAttached());
            prevState = true;
        }
        if (prevState == true && IsEnabled() == false) {
            fprintf(logFile, "</match>\n", logTime.Get());
            fflush(logFile);
            fclose(logFile);
            prevState = false;
        }

        if (IsEnabled())
            fprintf(logFile, "\t<entry time='%f' battery='%f' isTeleop='%d' isAuto='%d' >\n", logTime.Get(), ds->GetBatteryVoltage(), (int)IsOperatorControl(), (int)IsAutonomous());

        if (IsAutonomous())
        	Autonomous();
        else if (IsOperatorControl())
        	Teleop();
        else if(IsTest()){
            Test();
            //Ben();
        } else {
            // Disabled
        }

        if (IsEnabled()) {
            drive->log(logFile);
            fprintf(logFile, "\t</entry>\n");
        }

        /* Heartbeat signal. */
        if (counter % 100 == 0)
            ToggleRIOUserLED();
        counter++;
        Wait(0.01);
    }
}

START_ROBOT_CLASS(Rogue)

