
public class RepeatMain {
	
    private static String repeatSequence(String seq, int times){
	StringBuffer ret = new StringBuffer();
	
	for(int i = 0; i < times; i++)
	    ret.append(seq);
	
	return ret.toString();
    }
    
    public static void main(String argsp[]) throws Exception{
    	
    	int i = 0;
    	boolean up = true;
    	
    	while(true){
    		
    		
    		if(i % 50 == 0){
    			System.out.println(repeatSequence("()", i/50) + " " + i);		
    		}	
    			
    		if(i < 1000 && up)
    			i++;
    		else if(i >= 0){
    			up = false;
    			i--;
    		}else if(i < 0)
    			up = true;
    		
    		Thread.sleep(5);
    	}
    	
    	
    }
	
}
