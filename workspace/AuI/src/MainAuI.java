import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class MainAuI {

	public static void main(String asdf[]){
		JFrame j = new JFrame("Autonomous User Interface");
		ConsolePanel p = new ConsolePanel(asdf.length > 0 ? asdf[0] : "10.1.14.2");
		j.setContentPane(p);
		
		j.setSize(800,600);
		j.setVisible(true);
		j.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		j.setIconImage(new ImageIcon(new MainAuI().getClass().getResource("logo.png")).getImage());
		
		p.appendln("********* ROGUE AuI **********");
		p.appendln("****** for FRC team 114 ******");
		p.appendln("******** by Ben Evans ********\n");
		
		p.requestFocusForTextField();
//		p.connectToRobot();
		
	}
	
}
