import javax.swing.*;
import javax.swing.text.DefaultCaret;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Scanner;

public class ConsolePanel extends JPanel implements ActionListener, KeyListener{
	
	private JTextArea output;
	private JTextField input;
	private JScrollPane scroll;
	private ArrayList<String> commands = new ArrayList<String>();
	private int com = 0;
	private Client c;
	private ScriptPanel script = new ScriptPanel();
	private String ip;
	
	private class ScriptPanel extends JPanel implements ActionListener {
		private JTextArea script = new JTextArea(15,30);
		private JButton send = new JButton("GO!");
		private JScrollPane p = new JScrollPane(script);
		boolean before = false;
		
		public ScriptPanel(){
			
			script.setBackground(Color.BLACK);
			script.setForeground(Color.GREEN);
			script.setFont(new Font("COURIER", 0 ,14));
			
			p.setAutoscrolls(true);
			p.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
			
			send.addActionListener(this);
			
			setLayout(new BorderLayout());
			add(p, BorderLayout.CENTER);
			add(send, BorderLayout.SOUTH);
			
		}
		
		public void actionPerformed(ActionEvent e) {
			String[] b = script.getText().split("\n");
						
			for(String s : b){
				if(c != null && s != null){
					c.print(s);
				}
			}
		}
		
	}
	
	public ConsolePanel(String ip){
		
		this.setBackground(Color.BLACK);
		this.setOpaque(true);
		this.setLayout(new BorderLayout());
		this.ip = ip;
		
		//options for text area
		output = new JTextArea(50,50);
		output.setOpaque(true);
		output.setForeground(Color.GREEN);
		output.setBackground(Color.BLACK);
		output.setCaretColor(output.getBackground());
		output.setEditable(false);
		output.setFont(new Font("COURIER",0,14));
		output.setLineWrap(true);
		output.setWrapStyleWord(true);
		
		
		//makes so text panel can auto scroll
		DefaultCaret caret = (DefaultCaret)output.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);
		
		//making the window able to scroll
		scroll = new JScrollPane(output);
		scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scroll.setAutoscrolls(true);
		
		//options for text field
		input = new JTextField();
		input.addActionListener(this);
		input.addKeyListener(this);
		input.setFont(new Font("COURIER",0,14));
				
		
		//add components to panel
		add(scroll,BorderLayout.CENTER);
		add(input,BorderLayout.SOUTH);
		add(script, BorderLayout.EAST);
	}

	public void actionPerformed(ActionEvent e) {
		if(e.getSource().equals(input) && input != null){
			textInputted(input.getText());
		}
	}
	
	public void textInputted(String s) {
		appendln(s);
		commands.add(s);
		input.setText("");
		com = commands.size()-1;
		
		try{
			
			if(s.equalsIgnoreCase("clear"))
				clearScreen();
			else if(s.equalsIgnoreCase("exit"))
				System.exit(0);
			else if(s.equalsIgnoreCase("connect"))
				connectToRobot();
			else if(s.equalsIgnoreCase("disconnect"))
				disconnect();
			else if(s.startsWith("byte"))
				sendByte(s.substring(5));
			else if(s.equalsIgnoreCase("reddit"))
				Desktop.getDesktop().browse(new java.net.URI("http://reddit.com"));
			else
				c.println(s);
		
		}catch(Exception e){
			appendln("there was an error: " + e.getMessage());
		}
		
	}

	public void keyPressed(KeyEvent e) {
		
		if(e.getKeyCode() == KeyEvent.VK_UP && commands.size() > 0){
			input.setText(commands.get(com));
			com = com - 1 > 0 ? com - 1 : 0;
		}else if(e.getKeyCode() == KeyEvent.VK_DOWN && commands.size() > 0){
			com = com + 1 < commands.size() ? com+1 : com; 
			input.setText(commands.get(com));
		}
		
	}

	public void keyReleased(KeyEvent arg0) {
		
	}

	public void keyTyped(KeyEvent arg0) {
		
	}
	
	public void append(String s){
		output.append(s);
	}
	
	public void appendln(String s){
		append(s + "\n");
	}
	
	public void clearScreen(){
		output.setText("");
	}
	
	public void requestFocusForTextField(){
		input.requestFocus();
	}
	
	public void connectToRobot(){
		new Thread(){
			public void run(){
				try{
					append("connecting... ");
					c = new Client(ip, 1180);
					appendln("connected!");
					c.start();
					
				}catch(Exception e){
					appendln("could not connect to " + ip + ":1180");
				}
			}
		}.start();

	}
	
	public void disconnect() throws IOException{
		c.ps.close();
		c.sc.close();
		c.s.close();
		appendln("closed all connections!");
	}
	
	public void sendByte(String in) throws NumberFormatException{
		c.write(Byte.parseByte(in));
	}
	
	private class Client extends Thread{
		
		private Socket s;
		private Scanner sc;
		private PrintStream ps;
		
		
		public Client(String ip, int port)throws IOException,UnknownHostException{
			s = new Socket(ip,port);
			
			sc = new Scanner(s.getInputStream());
			ps = new PrintStream(s.getOutputStream());
			
		}
		
		public void print(String str){
			if(s != null && str != null && s.isConnected()){
				System.out.println("sent: "+str);
				ps.print(str);
			}else{
				appendln("Connected!");
			}
		}
		
		public void println(String str){
			print(str+"\n");
		}
		
		public void write(byte b){
			ps.write(b);
		}
		
		public void run(){
			while(s.isConnected() && sc.hasNextLine()){
				appendln(sc.nextLine());
			}
		}
		
	}
	
}
