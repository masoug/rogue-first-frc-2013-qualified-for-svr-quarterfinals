
#include <Winsock.h>
#include <stdio.h>

int main(){

	SOCKET s;
	WSADATA data;
	SOCKADDR_IN in;

	printf("starting up winsock!\n");

	if(WSAStartup(0x0202,&data))
		return 1;

	in.sin_family = AF_INET;
	in.sin_port = htons(1180);
	in.sin_addr.s_addr = htonl(INADDR_ANY);

	s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if(s == INVALID_SOCKET)
		return 2;

	printf("binding\n");

	if(bind(s, (LPSOCKADDR)&in, sizeof(in)) == SOCKET_ERROR)
		return 3;

	listen(s, 1);

	char buf[256];

	while(true){
		printf("waiting for client...\n");
		SOCKET client = accept(s,0,0);

		if(client == INVALID_SOCKET){
			closesocket(s);
			return 4;
		}

		printf("we got a client!\n");

		int received = 1;

		while(received != 0){
			memset(buf,0,sizeof(buf));
			received =  recv(s,(char *)buf,sizeof(buf),0);

			printf("received:\n%s",buf);

			if(buf[0] == 0 || buf[0] == -1)
				closesocket(client);

		}

	}

	WSACleanup();

	return 0;

}


