#ifndef CONFIG_H_
#define CONFIG_H_

/* Shooter */
#define SHOOTER_A 5
#define SHOOTER_B 4

/* Collector */
#define INTAKE 10
#define BOTTOM_INTAKE 2
#define DISPATCH 6

/* Lifter */
#define CLIMBER 7

/* Drivetrain */
#define GEAR 5
#define LEFT_FRONT 2
#define RIGHT_FRONT 9
#define LEFT_BACK 3
#define RIGHT_BACK 8
#define ENCODER_R_A 4
#define ENCODER_R_B 5
#define ENCODER_L_A 2
#define ENCODER_L_B 3

/* Compressor */
#define COMPRESSOR 1
#define PRESSURE 1

#endif
