
import org.apache.commons.net.ftp.*;
import java.io.*;
import java.net.*;


public class Robot {
	
	String ip;
	FTPClient ftp;
	Socket cRioOutput;
	InputStream is;
	
	public Robot(String ip){
		this.ip = ip;
		ftp = new FTPClient();
		cRioOutput = new Socket();
	}
	
	public boolean ftpLogon(){
		try {
			ftp.connect(ip);
			return ftp.login("", "");
		} catch (Exception e) {
			System.err.println("Could not connect or logon to ip: " + ip);
			return false;
		}
	}
	
	public boolean ncLogon(){
		try{
			cRioOutput = new Socket(ip, 6666);
			is = cRioOutput.getInputStream();
			return true;
		}catch(Exception e){
			System.err.println("Could not get cRio OutputStream!");
			return false;
		}
	}
	
	public InputStream getInputStream(){
		return is;
	}
	
	public boolean getFile(String remote, String local){
		try{
			File f = new File(local);
			f.createNewFile();
			return ftp.retrieveFile(remote, new FileOutputStream(f));
		}catch(Exception e){
			System.err.println("error retrieving file!");
			return false;
		}
	}
	
	public boolean deleteFile(String remote){
		try {
			return ftp.deleteFile(remote);
		} catch (IOException e) {
			System.err.println("error deleting file!");
			return false;
		}
	}
	
	public boolean sendFile(String remote, String local){
		try{
			File f = new File(local);
			if(!f.exists())
				return false;
			return ftp.storeFile(remote, new FileInputStream(f));
		}catch(Exception e){
			System.err.println("error sending file!");
			return false;
		}
	}
}
