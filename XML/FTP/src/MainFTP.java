
public class MainFTP {
	public static void main(String args[]){
		
		String ip = "10.1.14.2";
		String filename = "logFile.dat";
		
		if(args.length == 2){
			ip = args[0];
			filename = args[1];
		}else if(args.length != 0){
			System.err.println("usage: <ip> <file>");
		}
		
		
		Robot r = new Robot(ip);
		System.out.print("logging on... ");
		r.ftpLogon();
		System.out.println("done");
		System.out.print("getting file... ");
		if(!r.getFile(filename, filename)){
			System.err.println("could not get file... ");
			return;
		}
		System.out.print("deleting remote file... ");
		if(!r.deleteFile(filename)){
			System.err.println("could not delete file!");
			return;
		}
		System.out.println("done");
	}
}
