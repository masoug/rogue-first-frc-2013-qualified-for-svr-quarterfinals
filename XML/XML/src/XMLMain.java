import java.util.ArrayList;
import javax.swing.JFrame;
import org.math.plot.Plot2DPanel;

public class XMLMain {
	
	static boolean testing = false;
	
	public static void main(String args[]){
		
		String remote = "logFile.dat";
		String filename = String.format("Robot_%d_datafile.xml", System.currentTimeMillis());
		String ip = "10.1.14.2";
		boolean local = false;
		
		if(args.length == 3){
			ip = args[0];
			remote = args[1];
			filename = args[2];
		}else if(args.length == 1){
			filename = args[0]; //localfile not on robot
			local = true;
		}else{
			System.out.println("usage: XMLMain <localFile> or\n" +
								"XMLMain <ip> <remoteFile> <localFile>");
			return;
		}
					
		Robot r = new Robot(ip);
		
		if(!local && !r.getFile(remote, filename)){
			System.out.println("could not get file... exiting!");
			return;
		}
		
		
		LogFile f = new LogFile(filename);
		f.parse();
		
		if(!testing){
		
			Plot2DPanel p = new Plot2DPanel();
			p.addLegend("EAST");
			
			ArrayList<String> names = new ArrayList<String>();
			
			double[] xAsTime = new double[f.data.size()];
			double[] values = new double[f.data.size()];
			
			int i = 0;
			for(Entry e : f.data){
				xAsTime[i] = e.timeStamp;
				i++;
			}

			int j = 0;
			for(Entry e: f.data){
				for(Stream st: e.streams){
						values[j] = st.value;
				}	
				
				j++;
			}
			
			p.addLinePlot("test", xAsTime, new double[] {1,4,9}); 
				
			JFrame frame = new JFrame("Data plot");
			frame.setContentPane(p);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setVisible(true);
			frame.setSize(800, 600);
			
		}
	} 
	
}
