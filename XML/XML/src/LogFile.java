import java.io.File;
import java.util.ArrayList;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;


public class LogFile {
	
	File f;
	String contents = "";
	String lastLine = "";
	DocumentBuilder reader;
	Document doc;
	Element root;
	NodeList entries;
	
	ArrayList<Entry> data;
	
	public LogFile(String document){
		try{
			f = new File(document);
			reader = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			
			data = new ArrayList<Entry>();
		}catch(Exception e){}
		
	}
	
	public void parse(){
		try {
			doc = reader.parse(f);
			root = doc.getDocumentElement();
			entries = root.getChildNodes();
			
			for(int i = 0; i < entries.getLength(); i++) {
				
				Node currentEntry = entries.item(i);
				
				
				if(currentEntry.getNodeType() != Node.TEXT_NODE){
					
					double entryTime = Double.parseDouble(((Element)currentEntry).getAttribute("time"));
					boolean isAutonomous = currentEntry.getAttributes().getNamedItem("isAutonomous").getNodeValue().equals("1");
					Entry e = new Entry(entryTime, isAutonomous);
					
					// its BEN time!!!!!!!!!!!					
					NodeList modules = currentEntry.getChildNodes();
					
					for(int j = 0; j < modules.getLength(); j++){
						
						Node currentModule = modules.item(j);													
						
						if(currentModule.getNodeType() != Node.TEXT_NODE){
							
														
							NodeList streams = currentModule.getChildNodes();
																			
							for(int k = 0; k < streams.getLength(); k++){
																
								if(streams.item(k).getNodeType() != Node.TEXT_NODE){
									
									e.streams.add(new Stream(streams.item(k).getAttributes().getNamedItem("name").getNodeValue(),
												Double.parseDouble(streams.item(k).getAttributes().getNamedItem("value").getNodeValue()),
												streams.item(k).getAttributes().getNamedItem("type").getNodeValue()));
								}
							}
						}
					}
					data.add(e);
				}	
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("error parsing contents!");
		}
	}
}
