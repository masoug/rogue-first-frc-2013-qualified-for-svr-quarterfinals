
public class Stream {
	
	String name;
	double value;
	String type;
	
	public Stream(String name, double value, String type){
		this.name = name;
		this.value = value;
		this.type = type;
	}
	
	public String toString(){
		return String.format("name = %s value = %f type = %s",name,value,type);
	}
	
}
