import java.util.ArrayList;


public class Entry {
	
	double timeStamp;
	ArrayList<Stream> streams;
	boolean auto;
	
	public Entry(double timeStamp, boolean auto) {
		this.timeStamp = timeStamp;
		this.auto = auto;
		streams = new ArrayList<Stream>();
	}
	
	public String toString(){
		return String.format("time = %f auto? = %s %s", timeStamp, auto, streams);
	}
}
