#Rogue#
## Robot Code for Ultimate Ascent FIRST FRC Game ##

**Robot Name:** Rogue

**Description:**
Robot code to control the robot for FIRST FRC's Ultimate Ascent 2013 game.

**Modules:**
*Drive Module:* Base driving system that drives the robot's chassis. Includes velocity and directional support, i.e. the robot can drive at a set velocity and turn to arbitrary headings. There are also trapezoidal velocity curves that allow the robot to smoothey drive distances. Note: The units in the robot are metric, not imperial.

*Shooter:* Module that drives the shooting system; two motors launches the frisbees while a pneumatic piston dispatches the frisbees from the hopper.

*Climber:* Single, large-diameter cylinder that hooks the robot onto the pyramid and lifts the robot.

*Collector* Intake/belt system that picks up frisbees from the ground.

*Autonomous:* Finite state machine architecture that features a rudimentary scripting system that allows for flexible/on-demand development of autonomous routines.

**Additional documentation can be found in the "doc" directory!**

**Authors:**
Sammy Guo, Ben Evans, Michael Chin, Ankith Uppunda
