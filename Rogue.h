#ifndef ROGUE_H_
#define ROGUE_H_

#include <iostream>
#include "Commands.hpp"
#include "WPIlib.h"
#include "Modules/RobotModule.h"
#include "Modules/DriveModule.h"
//#include "Modules/Shooter.h"
#include "Modules/VelocityProfile.h"
#include "Modules/Shooter.h"
#include "AuI/AutoController.h"
#include "Config.h"
#include "Bezier.h"
#include "Xbox.h"

#define ROGUE_LCD(_message_) \
	dLCD->Clear(); \
	dLCD->PrintfLine(DriverStationLCD::kUser_Line1, _message_); \
	dLCD->UpdateLCD();

class Rogue : public SimpleRobot
{
public:
    Rogue(void);
    void RobotMain(void);

private:
	void Autonomous();
	void Teleop();
	void Test();
	void Ben();

	// logging
	FILE *logFile;
	Timer logTime;

	AutoController *autoControl;
	Instruction *autoInstruction;
	CommandType autoState;

	Victor *intake;
	Relay *bottomIntake;
	Relay::Value bottomIntakeDirection;
	// SHOOTER_MOTORS *shooterA;
	// SHOOTER_MOTORS *shooterB;
	Solenoid *climber;
	//Solenoid *dispatch;

	DriverStationLCD *dLCD;
	Compressor airCompressor;
	Joystick wheel;
	Joystick throttle;
	Joystick xBox;
	Xbox controller;

	/* Robot Modules. */
	DriveModule *drive;
	Shooter *shtr;
};

#endif
