%{
	#include <stdio.h>
	#include "AuI.hpp"

	int yylex(void);
%}

%%
#.*							/* comment */;
\-*[0-9\.]+					yylval.number = atof(yytext); return NUMBER;
drive						return TOKDRIVE;
shooter						return TOKSHOOTER;
fire						return TOKFIRE;
rotate						return TOKROTATE;
done						return TOKDONE;
wait						return TOKWAIT;
collect						return TOKCOLLECT;
curve						return TOKCURVE;
on|off						yylval.number = !strcmp(yytext, "on"); return BOOLEAN;
.							/* ignore everything else that doesn't match */;
\n 							/* ignore */;
[ \t]+						/* ignore */;
%%
