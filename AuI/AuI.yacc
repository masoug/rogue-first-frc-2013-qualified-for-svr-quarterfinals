%{
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <unistd.h>
#include "AutoController.h"
#include "WPILib.h"

extern "C" {
	int yyparse(void);
	int yylex(void);
	int yy_scan_string(const char *);
	int yylex_destroy(void);
	int yywrap() {
		//printf("AuI parse complete!\n");
		return 1;
	}
}

void yyerror(const char *str)
{
	fprintf(stderr,"AuI script parse error: %s\n",str);
}

extern FILE* yyin;
AutoController *controller = NULL;

void AutoController::control(AutoController *ptr) {
	AUIOUT << "AuI Server started..." << std::endl;
	controller = ptr;

	yyin = fopen("autonomous.aui", "r");
	if (!yyin) {
		AUIOUT << "AuI autonomous script not found. Going into server mode." << std::endl;

		struct sockaddr_in sockAddr;
		int socketFD = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (socketFD == -1) {
			perror("socket");
			return;
		}
		memset(&sockAddr, 0, sizeof(sockAddr));

		sockAddr.sin_family = AF_INET;
		sockAddr.sin_port = htons(1180);
		sockAddr.sin_addr.s_addr = INADDR_ANY;

		if (bind(socketFD, (struct sockaddr *)&sockAddr, sizeof(sockAddr)) == -1) {
			perror("bind");
			close(socketFD);
			return;
		}
		if (listen(socketFD, 10) == -1) {
			perror("listen");
			close(socketFD);
			return;
		}

		char buf[1024];
		while(true) {
			AUIOUT << "Waiting for a client..." << std::endl;
			int acceptFD = accept(socketFD, NULL, NULL);
			if (accept < 0) {
				perror("accept");
				close(socketFD);
				return;
			}
			AUIOUT << "Client found. Begin communication!" << std::endl;
			int bites = 1;
			while (bites != 0) {
				memset(buf, 0, sizeof(buf));
				bites = recv(acceptFD, (char *)&buf, sizeof(buf), 0);
				yy_scan_string((char *)&buf);
				yyparse();
				yylex_destroy();
				//send(acceptFD, "Message recieved.\n", 20, 0);
			}
			AUIOUT << "Client disconnected." << std::endl;
			if (shutdown(acceptFD, 2) == -1) {
				perror("shutdown");
			}
		}

	} else {
		AUIOUT << "AuI autonomous script found. Parsing..." << std::endl;
		yyparse();
		AUIOUT << "Finished parsing. Server exiting." << std::endl;
	}
}

%}

%token TOKDRIVE TOKSHOOTER TOKFIRE TOKROTATE TOKDONE TOKWAIT TOKCOLLECT TOKCURVE

%union
{
	double number;
}

%token <number> NUMBER
%token <number> BOOLEAN

%%

commands:
	| commands command
	;


command:
	drive | shooter | fire | rotate | done | wait | collect | curve
	;

drive:
	TOKDRIVE NUMBER NUMBER
	{
		printf("Driving %f meters at a max speed of %f m/s.\n", $2, $3);
		DriveCommand *command = new DriveCommand();
		command->distance = $2;
		command->velocity = $3;
		controller->add(command);
	}
	;

shooter:
	TOKSHOOTER BOOLEAN
	{
		ShooterCommand *command = new ShooterCommand();
		if ($2) {
			printf("Shooter on.\n");
			command->on = true;
		} else {
			printf("Shooter off.\n");
			command->on = false;
		}
		controller->add(command);
	}
	;

fire:
	TOKFIRE BOOLEAN
	{
		printf("fire\n");
		FireCommand *command = new FireCommand();
		if ($2) {
			command->fire = true;
		} else {
			command->fire = false;
		}
		controller->add(command);
	}
	;

rotate:
	TOKROTATE NUMBER
	{
		printf("Rotate to %f degrees.\n", $2);
		RotateCommand *command = new RotateCommand();
		command->degrees = $2;
		controller->add(command);
	}
	;

done:
	TOKDONE
	{
		printf("done\n");
		controller->add(new DoneCommand());
	}
	;

wait:
	TOKWAIT NUMBER
	{
		printf("wait\n");
		AuIWaitCommand *command = new AuIWaitCommand();
		command->time = $2;
		controller->add(command);
	}
	;

collect:
	TOKCOLLECT BOOLEAN
	{
		printf("collect\n");
		CollectCommand *command = new CollectCommand();
		if ($2) {
			command->collect = true;
		} else {
			command->collect = false;
		}
		controller->add(command);
	}
	;

curve:
	TOKCURVE NUMBER NUMBER NUMBER NUMBER NUMBER NUMBER NUMBER NUMBER NUMBER
	{
		printf("curve\n");
		controller->add(new CurveCommand( $2, $3, $4, $5, $6, $7, $8, $9, $10));
	}
	;

