#include "AutoController.h"
#include <assert.h>
#include "Synchronized.h"

AutoController::AutoController(): 
	m_task("AutoController", (FUNCPTR)runControl), m_commandQueue()
{
	AUIOUT << "Initializing..." << std::endl;
	m_semaphore = semMCreate(SEM_Q_PRIORITY);
	m_task.Start((INT32)this);
}

void AutoController::add(Instruction *i) {
	//printf("Added instruction\n");
	Synchronized s(m_semaphore);
	m_commandQueue.push(i);
}

Instruction* AutoController::pop() {
	//printf("Instruction popped\n");
	Synchronized s(m_semaphore);
	Instruction *result = m_commandQueue.front();
	m_commandQueue.pop();
	return result;
}

bool AutoController::empty() {
	Synchronized s(m_semaphore);
	return m_commandQueue.empty();
}

void AutoController::clear() {
	Synchronized s(m_semaphore);
	while (!m_commandQueue.empty())
		m_commandQueue.pop();
}

void AutoController::runControl(AutoController *ptr) {
	ptr->control(ptr);
}

AutoController::~AutoController() {
}
