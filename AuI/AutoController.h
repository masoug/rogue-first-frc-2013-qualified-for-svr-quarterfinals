#ifndef AUTOCONTROLLER_H_
#define AUTOCONTROLLER_H_

#include <iostream>
#include <queue>
#include <string>
#include "../Commands.hpp"
#include "Base.h"
#include "Task.h"
#include "semLib.h"

#define AUIOUT std::cout << "[AuI Server] "

/*
 * Abstract interface class for robot modules.
 */
class AutoController {
public:
	AutoController();
	void add(Instruction *i);
	Instruction* pop();
	bool empty();
	void clear();
	virtual ~AutoController();

private:
	virtual void control(AutoController *ptr);

	Task m_task;
	SEM_ID m_semaphore;
	std::queue<Instruction *> m_commandQueue;

	static void runControl(AutoController *ptr);
	DISALLOW_COPY_AND_ASSIGN(AutoController);
};

#endif /* AUTOCONTROLLER_H_ */
