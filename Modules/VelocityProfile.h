#ifndef VELOCITY_PROFILE_H_
#define VELOCITY_PROFILE_H_

#include <string>

class VelocityProfile {

public:
	VelocityProfile(double distance, double maxVelocity);
	VelocityProfile(double t1, double t2, double t3, double maxVelocity);
	~VelocityProfile();

	double eval(double t);
	double totalTime();
	std::string toString();

private:
	double m_maxVelocity;
	double m_t1, m_t2, m_t3;

};

#endif
