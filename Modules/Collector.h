#ifndef COLLECTOR_H_
#define COLLECTOR_H_

#include "WPILib.h"
#include "RobotModule.h"

class Collector : public RobotModule
{
public: 
	Collector(int motor, int sensor);
	~Collector();
	void cmc();
private:
	// varables
	//put in variables here as we go
	void motorstart();
	void motorstop();
	void control();
	void addfris();
	void count_fo_dayzzz();
	enum CollectorState {
		IDLE,
		ACTIVE,
	};
	CollectorState C_State;
	bool m_fris;
	int amountfrisbies;
	bool Collect;
	int prevfris;
	float counts;

	// Objects
	Victor m_motor;
	//Timer C_counter;
	//DigitalInput hoppersensor;
};
#endif 
