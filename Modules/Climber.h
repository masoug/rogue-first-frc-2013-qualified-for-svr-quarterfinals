#ifndef CLIMBER_H_
#define CLIMBER_H_

#include "WPILib.h"
#include "RobotModule.h"

class Climber : public RobotModule {
public:
	Climber();
	void enable(bool en=true);
	virtual ~Climber();

private:
	void control();
	void hook();
	void S_up();
	void S_down();
	// void loRetr();
	// void loExt();
	// void hiRetr();
	// void hiExt();
	// void hiEng();
	// void tiltBack();
	// void tiltAdduct();

	/* Climber state */
	// enum ClimbState {
	// 	IDLE, // Idle state
	// 	ABORT, // abort!
	// 	L_ENG, // low hook engage
	// 	H_EXT, // high hook extend
	// 	H_ADT, // high hook adduct
	// 	H_ENG, // high hook engage
	// 	H_RTR, // high hook retract
	// 	L_EXT, // low hook extend
	// 	H_RLX // high hook relax
	// };
	// ClimbState m_state;

	

	/* Sensors! */
	// DigitalInput m_loTouch, m_loEngaged, m_loExtended;
	// DigitalInput m_hiTouch, m_hiEngaged;
	// DigitalInput m_hiExtended, m_hiRetracted; /* virtual "sensor" that detects swinging in the robot */

	/* Hardware */
	// Victor m_highHook;
	// Solenoid m_lowHook;
	Solenoid m_tilt;
};

#endif /* ROBOT_MODULE_H_ */
