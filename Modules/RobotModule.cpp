#include "RobotModule.h"
#include <assert.h>

RobotModule::RobotModule(std::string name): 
	m_name(name), m_task(name.c_str(), (FUNCPTR)runControl)
{
	MODOUT << "Initializing..." << std::endl;
	m_semaphore = semMCreate(SEM_Q_PRIORITY);
}

void RobotModule::runControl(RobotModule *ptr) {
	ptr->control();
}

void RobotModule::control() {
	MODOUT << "Control not overloaded." << std::endl;
}

RobotModule::~RobotModule() {
}
