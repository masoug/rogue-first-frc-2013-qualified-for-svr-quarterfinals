#ifndef SHOOTER_H_
#define SHOOTER_H_

#include "WPILib.h"
#include "RobotModule.h"

/* Change these with practice/competition robots */
#define SHOOTER_MOTORS Talon

class Shooter : public RobotModule {
public:
	// Shooter();
	// Shooter(int sh);
	Shooter(int solenoid, int motorA, int motorB);
	~Shooter();

	/* Getters and setters. REQUIRES SEMAPHORES!*/
	//void fire(bool f=true);
	/* END GETTERS AND SETTERS. */
	//void go();
	void pistonRetract();
	void setPower(double pwr);
	void wheelStart();
	void wheelStop();
	void pistonExtend();
	
private:
	double m_power;
	void control();
	void countFrisbees();

	enum ShooterState {
		IDLE,
		ACTIVE,
		LAUNCH
	};

	ShooterState m_state;
	bool m_frisA;
	/*double m_rpmB;
	double m_rpmBA;*/
	bool m_fire;
	int prev;
	float time;
	//"will intertwine with collector class" -Ankith
	//int amountFrisbies;


	/* Hardware */
	SHOOTER_MOTORS m_motorA;
	SHOOTER_MOTORS m_motorB;
	Solenoid m_solenoid;
	Timer leTime;
	
};

#endif
