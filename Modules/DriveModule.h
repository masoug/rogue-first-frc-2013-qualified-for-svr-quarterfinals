#ifndef DRIVE_MODULE_H_
#define DRIVE_MODULE_H_

#include "WPILib.h"
#include "RobotModule.h"

/* 254's victor linearizer. */
float victor_linearize(double goal_speed);

class PIDReader : public PIDOutput {
public:
	float out;
	void PIDWrite(float output) {out = output;}
};

/* Basic example of robot modules. */
class DriveModule : public RobotModule {
public:
	DriveModule(int lf, int lb, int rf, int rb, int lea, int leb, int rea, int reb, int gear);
	void enable(bool en = true);
	~DriveModule();

	/* Getters and setters. REQUIRES SEMAPHORES!*/
	void setVector(double velocity, double angle);
	/* Convenience function that converts joysticks to 
	* setVector() vector format and calls setVector.
	* Do not grab the semaphore with this function!!! */
	void joystickDrive(double x, double y);
	void setAnglePD(double p, double d);
	void setVelocityPD(double p, double d);
	void setTune(double l, double r);
	void manual(bool man=true);
	void log(FILE *fp);
	double getAngleError();
	void gear(bool lo=true);
	double getDistance();
	void resetDistance();
	/* END GETTERS AND SETTERS. */

private:
	void control();
	void drive(double left, double right);

	/* Hardware */
	Victor m_leftFront;
	Victor m_leftBack;
	Victor m_rightFront;
	Victor m_rightBack;
	Solenoid m_gear;
	Encoder m_lEncoder, m_rEncoder;
	Gyro m_gyro;

	/* Angle Controllers */
	PIDReader m_rAngle;
	PIDController m_cAngle;

	/* Control variables */
	bool m_enabled, m_manual;
	double m_lTune, m_rTune;
	double m_tVelocity, m_tAngle; /* Target velocity and target angle. */
	double m_currVelocity;
	double m_pVelocity, m_dVelocity;
	double m_pout, m_dout, m_fout, m_out, m_angleOut;
	double m_leftPower, m_rightPower;
	double m_currDistance, m_goalDistance;
};

#endif
