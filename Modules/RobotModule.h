#ifndef ROBOT_MODULE_H_
#define ROBOT_MODULE_H_

#include <iostream>
#include <string>
#include "Base.h"
#include "Task.h"
#include "semLib.h"

#define MODOUT std::cout<<"["<<m_name<<"] "

/*
 * Abstract interface class for robot modules.
 */
class RobotModule {
public:
	RobotModule(std::string name);
	virtual ~RobotModule();

protected:
	virtual void control();

	std::string m_name;
	Task m_task;
	SEM_ID m_semaphore;

private:
	static void runControl(RobotModule *ptr);
	DISALLOW_COPY_AND_ASSIGN(RobotModule);
};

#endif /* ROBOT_MODULE_H_ */
