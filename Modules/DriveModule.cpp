#include "DriveModule.h"
#include <assert.h>
#include <math.h>

#define METERS_PER_COUNT 0.0017954202

DriveModule::DriveModule(int lf, int lb, int rf, int rb, int lea, int leb, int rea, int reb, int gear) :
RobotModule("DriveModule"),
m_leftFront(lf), m_leftBack(lb), m_rightFront(rf), m_rightBack(rb), m_gear(gear),
m_lEncoder(lea, leb), m_rEncoder(rea, reb), m_gyro(1),
m_cAngle(0.0, 0.0, 0.0, &m_gyro, &m_rAngle),
m_enabled(false), m_manual(false), m_lTune(1.0), m_rTune(1.0), m_tVelocity(0.0), m_tAngle(0.0),
m_currVelocity(0.0), m_pVelocity(0.0), m_dVelocity(0.0),
m_pout(0.0), m_dout(0.0), m_fout(0.0), m_out(0.0), m_angleOut(0.0), m_leftPower(0.0), m_rightPower(0.0)
{
	assert(m_task.Start((INT32)this) == 1);
	m_lEncoder.Start();
	m_rEncoder.Start();
	m_lEncoder.SetReverseDirection(true);
	m_rEncoder.SetReverseDirection(false);
	m_cAngle.Enable();
	//m_cAngle.SetContinuous(true);
	m_cAngle.SetTolerance(1.0);
	m_cAngle.SetOutputRange(-1.0, 1.0);
	
}

void DriveModule::enable(bool en) {
	Synchronized s(m_semaphore);
	m_enabled = en;
}

void DriveModule::joystickDrive(double x, double y) {
	Synchronized s(m_semaphore); // must have mutexes
	if(x == 0 && y == 0){
		m_tVelocity = 0;
		m_tAngle = 0;
		return;
	}
	if(y >= 0)
		m_tVelocity = sqrt(fabs(x*x) + fabs(y*y)); //a^2 + b^2 = c^2
	else
		m_tVelocity = -1.0 * sqrt(fabs(x*x) + fabs(y*y));

	if(x != 0)
		m_tAngle = 90.0 - atan(y/x/3.141592653)*180.0; //sohcahtoa
	else
		m_tAngle = 0;
}

void DriveModule::setAnglePD(double p, double d) {
	Synchronized s(m_semaphore);
	m_cAngle.SetPID(p, 0.0, d);
}

void DriveModule::setVelocityPD(double p, double d) {
	Synchronized s(m_semaphore);
	m_pVelocity = p;
	m_dVelocity = d;
}

void DriveModule::setTune(double l, double r) {
	Synchronized s(m_semaphore);
	m_lTune = l;
	m_rTune = r;
}

void DriveModule::manual(bool man) {
	Synchronized s(m_semaphore);
	m_manual = man;
}

void DriveModule::log(FILE *fp) {
	Synchronized s(m_semaphore);
	fprintf(fp, "    <module name='DriveModule'>\n");
	fprintf(fp, "      <stream name='tVelocity' value='%f' type='float' />\n", m_tVelocity);
	fprintf(fp, "      <stream name='tAngle' value='%f' type='float' />\n", m_tAngle);
	fprintf(fp, "      <stream name='currDistance' value='%f' type='float' />\n", m_currDistance);
	fprintf(fp, "      <stream name='goalDistance' value='%f' type='float' />\n", m_goalDistance);
	fprintf(fp, "      <stream name='currVelocity' value='%f' type='float' />\n", m_currVelocity);
	fprintf(fp, "      <stream name='pout' value='%f' type='float' />\n", m_pout);
	fprintf(fp, "      <stream name='dout' value='%f' type='float' />\n", m_dout);
	fprintf(fp, "      <stream name='fout' value='%f' type='float' />\n", m_fout);
	fprintf(fp, "      <stream name='vout' value='%f' type='float' />\n", m_out);
	fprintf(fp, "      <stream name='leftPower' value='%f' type='float' />\n", m_leftPower);
	fprintf(fp, "      <stream name='rightPower' value='%f' type='float' />\n", m_rightPower);
	fprintf(fp, "      <stream name='angleOut' value='%f' type='float' />\n", m_angleOut);
	fprintf(fp, "      <stream name='angle' value='%f' type='float' />\n", m_gyro.GetAngle());
	fprintf(fp, "    </module>\n");
}

double DriveModule::getAngleError() {
	Synchronized s(m_semaphore);
	return fabs(m_tAngle - m_gyro.GetAngle());
}

void DriveModule::gear(bool lo) {
	m_gear.Set(!lo); // change as needed
}

double DriveModule::getDistance() {
	Synchronized s(m_semaphore);
	return m_currDistance;
}

void DriveModule::resetDistance() {
	Synchronized s(m_semaphore);
	m_goalDistance = 0.0;
	m_lEncoder.Reset();
	m_rEncoder.Reset();
}

void DriveModule::setVector(double velocity, double angle) {
	Synchronized s(m_semaphore);
	m_tVelocity = velocity;
	m_tAngle = angle;
}

void DriveModule::drive(double left, double right) {

	double dif = 0;

	if(left > 1.0 || right > 1.0){
		dif = max(right, left) - 1.0;
	}else if(left < -1.0 || right < -1.0){
		dif = min(right, left) + 1.0;
	}

	left -= dif;
	right -= dif;
	left *= m_lTune;
	right *= m_rTune;
	left = victor_linearize(left);
	right = victor_linearize(right);

	m_leftFront.Set(left);
	m_leftBack.Set(left);
	m_rightFront.Set(right);
	m_rightBack.Set(right);
}

void DriveModule::control() {
	int i = 0;
	double prevDistance, prevTime;
	Timer timer;
	timer.Start();
	prevTime = 0.0;

	while (true) {
		CRITICAL_REGION(m_semaphore)
		/* If we are enabled */
		if (m_enabled) {

			m_currDistance = METERS_PER_COUNT*(m_lEncoder.Get()+m_rEncoder.Get())/2.0;
			double currTime = timer.Get();
			double m_currVelocity = (m_currDistance-prevDistance) / (currTime-prevTime);
			if (fabs(m_currVelocity) > 10.0) // temporary fix to the sudden spike in m_currVelocity at the beginning.
				m_currVelocity = 0.0;
			m_goalDistance += m_tVelocity*(currTime-prevTime);

			// Set the turn control system.
			m_cAngle.SetSetpoint(m_tAngle);

			m_pout = (m_pVelocity) * (m_goalDistance-m_currDistance);
			m_dout = (m_dVelocity) * (m_tVelocity-m_currVelocity);
			m_fout = (0.0) * m_tVelocity; // + kf * goal acceleration
			m_out = m_pout+m_dout+m_fout;
			m_out = m_out > 1.0 ? 1.0 : (m_out < -1.0 ? -1.0 : m_out);
			m_angleOut = m_cAngle.Get();

			m_leftPower = m_out+m_angleOut;
			m_rightPower = m_out-m_angleOut;

			if (m_manual) /* Manual driving */
				drive(m_tVelocity+m_tAngle, m_tVelocity-m_tAngle);
			else
				drive(m_leftPower, m_rightPower);

			prevDistance = m_currDistance;
			prevTime = currTime;

			if (i % 50 == 0) {
				 //MODOUT << "l = " << m_lEncoder.Get() << " r = " << m_rEncoder.Get() << " ang = " << m_gyro.GetAngle() << std::endl;
				//MODOUT << "P=" << m_cAngle.GetP() << " D=" << m_cAngle.GetD() << " G=" << m_gyro.GetAngle() << std::endl;
			}
			i++;
		} else { /* We are disabled */
			m_goalDistance = 0.0;
			m_gyro.Reset();
			m_lEncoder.Reset();
			m_rEncoder.Reset();
			drive(0.0, 0.0);
		}
		END_REGION
		Wait(0.005);
	}
}

DriveModule::~DriveModule() {
	/* Destructor */
}

/* 254's victor linearizer. */
float victor_linearize(double goal_speed)
{
	const double deadband_value = 0.082;
	if (goal_speed > deadband_value)
		goal_speed -= deadband_value;
	else if (goal_speed < -deadband_value)
		goal_speed += deadband_value;
	else
		goal_speed = 0.0;
	goal_speed = goal_speed / (1.0 - deadband_value);

	double goal_speed2 = goal_speed * goal_speed;
	double goal_speed3 = goal_speed2 * goal_speed;
	double goal_speed4 = goal_speed3 * goal_speed;
	double goal_speed5 = goal_speed4 * goal_speed;
	double goal_speed6 = goal_speed5 * goal_speed;
	double goal_speed7 = goal_speed6 * goal_speed;

	// Original untweaked one.
	//double victor_fit_c		= -1.6429;
	//double victor_fit_d		= 4.58861e-08;
	//double victor_fit_e		= 0.547087;
	//double victor_fit_f		= -1.19447e-08;

	// Constants for the 5th order polynomial
	double victor_fit_e1		= 0.437239;
	double victor_fit_c1		= -1.56847;
	double victor_fit_a1		= (- (125.0 * victor_fit_e1  + 125.0 * victor_fit_c1 - 116.0) / 125.0);
	double answer_5th_order = (victor_fit_a1 * goal_speed5
		+ victor_fit_c1 * goal_speed3
		+ victor_fit_e1 * goal_speed);

	// Constants for the 7th order polynomial
	double victor_fit_c2 = -5.46889;
	double victor_fit_e2 = 2.24214;
	double victor_fit_g2 = -0.042375;
	double victor_fit_a2 = (- (125.0 * (victor_fit_c2 + victor_fit_e2 + victor_fit_g2) - 116.0) / 125.0);
	double answer_7th_order = (victor_fit_a2 * goal_speed7
		+ victor_fit_c2 * goal_speed5
		+ victor_fit_e2 * goal_speed3
		+ victor_fit_g2 * goal_speed);


	// Average the 5th and 7th order polynomials
	double answer =  0.85 * 0.5 * (answer_7th_order + answer_5th_order)
	+ .15 * goal_speed * (1.0 - deadband_value);

	if (answer > 0.001)
		answer += deadband_value;
	else if (answer < -0.001)
		answer -= deadband_value;

	return answer;
}
