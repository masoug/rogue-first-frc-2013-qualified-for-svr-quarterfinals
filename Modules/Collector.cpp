#include "Collector.h"
#include <assert.h>
#include <math.h>

Collector::Collector(int motor, int sensor):
//sensors ports
//ports 
//objects
RobotModule("Collector"),
m_fris(false), amountfrisbies(0), Collect(false),
m_motor(motor)
//hoppersensor(sensor)
{
	/* Constructor */
	C_State = IDLE;

};
//void Collector::count_fo_dayzzz() {
	//C_counter.Start();
	//counts = C_counter.Get();
//}

void Collector::cmc() {
	Synchronized s(m_semaphore);
	Collect = true;
}

void Collector::motorstart() {
	m_motor.Set(1.0);
}

void Collector::motorstop(){
	m_motor.Set(0.0);
	/*stop = drive->setVector(-1.0,0.0);
	if(stop == 0)
	{
		stop = drive->setVector(0.0,0.0)
	}*/

}

// void Collector::addfris() {

// 	prevfris = amountfrisbies;
// 	if (hoppersensor.Get() && !m_fris) {	
// 		m_fris = true;
// 		amountfrisbies++;
// 	} else if (!hoppersensor.Get()) {	
// 		m_fris = false;
// 	}	
	/*else if (shootersensor.Get() && !m_frisA)
	{	m_frisA = true ;
		amountfrisbies--;
	}	
	else if (!shootersensor.Get())
	{	m_frisA = false;

	}*/
// }

void Collector::control() {
	Collect = false;

	while(true) {

		CRITICAL_REGION(m_semaphore)
		if(C_State == IDLE){
			 motorstop();
			 if(Collect == true) {
			 	C_State = ACTIVE;
			 }
		}
		else if(C_State == ACTIVE) {

			if(amountfrisbies == 4) {
				C_State = IDLE;
			} else {
				//count_fo_dayzzz();
				motorstart();
				//addfris();
			}
				
			//if(amountfrisbies == prevfris + 1 || counts >= 7.0) {
					C_State = IDLE;
					//C_counter.Stop();	
			//}
		}
		END_REGION
	}
}

Collector::~Collector() {
	/* destructor */
}
