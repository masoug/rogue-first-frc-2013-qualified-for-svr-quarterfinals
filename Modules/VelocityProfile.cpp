#include "VelocityProfile.h"
#include <sstream>

VelocityProfile::VelocityProfile(double distance, double maxVelocity) :
	m_maxVelocity(maxVelocity),
	m_t1(0.0), m_t2(0.0), m_t3(0.0)
{
	double unitTime = distance/(2.0*maxVelocity);
	m_t1 = unitTime;
	m_t2 = m_t1+unitTime;
	m_t3 = m_t2+unitTime;
}

VelocityProfile::VelocityProfile(double t1, double t2, double t3, double maxVelocity) :
	m_maxVelocity(maxVelocity),
	m_t1(t1), m_t2(t2), m_t3(t3)
{
	//ctor
}

double VelocityProfile::eval(double t) {
	if (t > 0 && t <= m_t1)
		return (m_maxVelocity/m_t1)*t;
	else if (t > m_t1 && t <= m_t2)
		return m_maxVelocity;
	else if (t > m_t2 && t < m_t3)
		return ((-m_maxVelocity/m_t1)*(t-m_t2))+m_maxVelocity;
	else
		return 0.0;
}

double VelocityProfile::totalTime() {
	return m_t3;
}

std::string VelocityProfile::toString() {
	std::stringstream output;

	output << "Velocity profile that goes " << m_maxVelocity
	<< " m/s at critical times " << m_t1 << ", " << m_t2 << ", " << m_t3
	<< " that will travel " << (0.5*(m_t3+m_t1)*m_maxVelocity) << " meters.";

	return output.str();
}

VelocityProfile::~VelocityProfile() {
	/* destruct */
}
