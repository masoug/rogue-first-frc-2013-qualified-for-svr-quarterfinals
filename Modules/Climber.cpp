#include "Climber.h"

Climber::Climber() :
RobotModule("Climber"),
climb_start,
m_tilt(),

{
	/* Constructor. */
	m_state = IDLE;
}
void Climber::climber_mechanism()
{
	climb_start = true;
}
void Climber::S_up()
{
	m_tilt.set(1);

}
void Climber::S_down()
{
	m_tilt.set(0);

/* We need to program the climber to work with the Xbox controller 
and need to decide if we are going to use just 1 or 2 buttons or a joystick
on the Xbox controller to control the up and down motion of the climber */

}


Climber::~Climber() {
	/* Destructor */
}

