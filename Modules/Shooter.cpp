#include "Shooter.h"
#include <assert.h>
#include <math.h>

#define TARGET_SPEED 0.0


//Shooter::Shooter(int sh, int dp, int sol, int ea, int eb, int ln) :
Shooter::Shooter(int solenoid, int motorA, int motorB) :
RobotModule("Shooter"),
m_power(1.0),
m_motorA(motorA), m_motorB(motorB),
m_solenoid(solenoid), leTime()
{
	/* Constructor */
	//assert(m_task.Start((INT32)this) == 1);
	m_motorA.SetSafetyEnabled(false);
	m_motorB.SetSafetyEnabled(false);

}

void Shooter::countFrisbees() {
	// if (shootersensor.Get() && !m_frisA) {
	// 	m_frisA = true ;
	// 	amountFrisbies--;
	// } else if (!shootersensor.Get()){
	// 	m_frisA = false;
	// }
}	
// void Shooter::fire(bool f) {
// 	//Synchronized s(m_semaphore);
// 	//m_fire = f;
// }

void Shooter::wheelStop() {
	m_motorA.Set(0.0);
	m_motorB.Set(0.0);
}

void Shooter::wheelStart() {
	m_motorA.Set(m_power);
	m_motorB.Set(m_power);
}

void Shooter::setPower(double pwr) {
	m_power = pwr;
}

void Shooter::pistonRetract() {
	m_solenoid.Set(false);
}

void Shooter::pistonExtend() {
	m_solenoid.Set(true);
	//amountFrisbies--;
}

// void Shooter::go(){
// 	//m_fire == rJoy.GetTrigger();
// }
	
void Shooter::control() {
	/* Control loop */
	// int i = 0;
	// while (true) {
	// 	CRITICAL_REGION(m_semaphore)
	// 	if (m_state == IDLE) {

	// 		leTime.Stop();
	// 		leTime.Reset();

	// 		if (m_fire) { // need to keep track of number of frisbees!
	// 			m_state = ACTIVE;
	// 			wheelStop(); 
	// 			pistonRetract();
	// 		} else {
	// 			wheelStop();
	// 			pistonRetract();
	// 		}

	// 	} else if (m_state == ACTIVE) {
	// 		//countFrisbees();
	// 		m_state = LAUNCH;

	// 	} else if (m_state == LAUNCH) {
	// 		// warning! possible to be perpetually stuck in  
	// 		// this state without a timeout!

	// 		leTime.Start();

	// 		wheelStart();
	// 		pistonExtend();
	// 	}
	// 	END_REGION
	// 	Wait(0.005);
	// 	i++;
	// }
}

Shooter::~Shooter() {
	/* Destructor */
}

